<?php

    require_once('../models/conexion.php');
    $oJson = '';
    $sConexion = new conexion();
    $oMesas = null;
    $oComandas = null;

    $sQuery = "select * from cmesas";
    if ($sConexion->conectar()){
        $oMesas = $sConexion->consultaRetorno($sQuery);
        $sQuery = "SELECT * FROM tmpcomanda";
        $oComandas = $sConexion->consultaRetorno($sQuery);
    }
    if($oMesas){
        $oJson = '{
            "success": true,
            "mesas": [';

                foreach($oMesas as $mesa => $val){
                    $oJson = $oJson.'{
                        "Id": '.$val[0].',
                        "Nombre": "'.$val[1].'",
                        "Seccion": "'.$val[2].'",
                        "IdMesero": '.$val[3].',
                        "Ocupada": '.$val[4].',
                        "Unidas": '.$val[5].',
                        "ReqFac": '.$val[6].',
                        "NumMesa": '.$val[7].'
                    },';
                }
                $oJson = substr($oJson,0,-1);
                if($oComandas){
                    $oJson = $oJson.'],
                    "comandas": [';
                    foreach($oComandas as $comanda => $val){
                        $oJson = $oJson.'{
                            "IdPro": "'.$val[0].'",
                            "Precio": '.$val[3].',
                            "IdMesa": '.$val[4].',
                            "IdMesero": '.$val[5].',
                            "NoComensales": '.$val[6].',
                            "NoComensal": '.$val[7].'
                        },';
                    }
                    $oJson = substr($oJson, 0, -1);
                    $oJson = $oJson.']}';
                }else{
                    $oJson = $oJson.'],
                    "comandas": "No hay comandas aun"}';
                }
    }else{
        $oJson = '{
            "success": false,
            "mesas": "No hay mesas",
            "coamndas": "No se pueden tener coamandas"
        }';
    }

    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
    header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
    header('content-type: application/json; charset=utf-8');
    echo $oJson;