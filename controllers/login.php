<?php
    require_once('../models/conexion.php');

    $postdata = file_get_contents("php://input");
    $request = json_decode($postdata);
    $oCon = new conexion(); 
    $user = $request->user;
    $aDatos = null;
    $oJson = '';

    if($oCon->conectar()){
        $sQuery = "SELECT * FROM cmeseros WHERE Cve=$user";
        $aDatos = $oCon->consultaRetorno($sQuery);
        $oCon->desconectar();
    }

    if($aDatos) {
        $oJson = '{
            "success": true,
            "datos": {
                "id": "'.$aDatos[0][0].'",
                "Cve": "'.$aDatos[0][1].'",
                "nombre": "'.$aDatos[0][2].'",
                "Telefono": "'.$aDatos[0][3].'",
                "Ciudad": "'.$aDatos[0][4].'",
                "TipUsr": "'.$aDatos[0][5].'"
            }
        }';
    }else{
        $oJson = '{
            "success": false,
            "datos": "Usuario invalido"
        }';
    }
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
    header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
    header('content-type: application/json; charset=utf-8');
    echo $oJson;

