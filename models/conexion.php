<?php
/****************************************************************************************
 * clase conexion para acceso ala base de datos en MySQL
 * Autor: Joanan Sierra Sánchez
****************************************************************************************/
class conexion{
    private $sCon = null;

    function conectar() {
        $estado = false;
        try {
            $this->sCon = new PDO('mysql:host=localhost;dbname=sir','root','',  array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'UTF8'"));
            $estado = true;
        } catch(Exception $e) {
            throw $e;
        }
        return $estado;
    }

    function desconectar() {
        $this->sCon = null;
    }

    function consultaRetorno($sQuery) {
     $resul = null;
     $datos = null;
     $i = 0;
     $j = 0;
     if ($sQuery == null)
        throw new Exception("Error al ejecutar consulta con retorno <-----> La consulta esta vacia");
     if ( $this->sCon == null)
        throw new Exception("Error al ejecutar consulta con retorno <-----> No se ha creado la conexion");
     try {
         $resul = $this->sCon->query($sQuery);
     } catch(Exception $e) {
         throw $e;
     }
     if ($resul){
  			foreach ($resul as $linea) {
  				foreach($linea as $columna => $valColumna){
  					if(is_string($columna)){
  						$datos[$i][$j] = $valColumna;
  					    $j++;
  					}
  				}
  				$j = 0;
  				$i++;
  			}
  	}
     return $datos;
    }

    function consultaJson($sQuery) {
        $resul = null;
     $datos = null;
     $i = 0;
     $j = 0;
     if ($sQuery == null)
        throw new Exception("Error al ejecutar consulta con retorno <-----> La consulta esta vacia");
     if ( $this->sCon == null)
        throw new Exception("Error al ejecutar consulta con retorno <-----> No se ha creado la conexion");
     try {
         $resul = $this->sCon->query($sQuery);
     } catch(Exception $e) {
         throw $e;
     }

     $total_column = $resul->columnCount();
//var_dump($total_column);
for ($counter = 0; $counter < $total_column; $counter ++) {
    $meta = $resul->getColumnMeta($counter);
    $column[] = $meta['name'];
}
//print_r($column);

    }

    function consultaSimple($sQuery) {
        $afectados = -1;
        if($sQuery == null)
            throw new Exception('Error en la consulta simple <----> La consulta esta vacia.');
        if($this->sCon == null)
            throw new Exception('Error en la consulta simple <----> No se ha creado la conexion.');
        try {
            $afectados = $this->sCon->exec($sQuery);
        } catch(Exception $e){
            throw $e;
        }
    }
}